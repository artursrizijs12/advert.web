﻿using Advert.Web.Models.Validations;
using System.ComponentModel.DataAnnotations;

namespace Advert.Web.Models
{
    public class RegistrationViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }

        [Required]
        [PhoneNumberValidation]
        public string Phone { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Wrong email format")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The passwords dont match")]
        public string ConfirmPassword { get; set; }
    }
}
