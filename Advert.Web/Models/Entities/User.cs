﻿using Microsoft.AspNetCore.Identity;

namespace Advert.Web.Models.Entities
{
    public class User : IdentityUser
    {
        public string Name { get; set; }
        public string Surname {  get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
    }
}
