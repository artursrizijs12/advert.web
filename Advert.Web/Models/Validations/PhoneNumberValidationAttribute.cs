﻿using System.ComponentModel.DataAnnotations;
using PhoneNumbers;

namespace Advert.Web.Models.Validations
{
    public class PhoneNumberValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var phoneNumber = value as string;
            if (string.IsNullOrWhiteSpace(phoneNumber)) 
            {
                return new ValidationResult("Phone is mandatory field");
            }

            var phoneNumberUtil = PhoneNumberUtil.GetInstance();
            try
            {
                var numberProto = phoneNumberUtil.Parse(phoneNumber, null);
                if (!phoneNumberUtil.IsValidNumber(numberProto))
                {
                    return new ValidationResult("Incorrect phone number");
                }
            }
            catch (NumberParseException)
            {
                return new ValidationResult("Incorrect phone number format");
            }

            return ValidationResult.Success;
        }
    }
}
