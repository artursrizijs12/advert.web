﻿using System.Threading.Tasks;

namespace Advert.Web.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
